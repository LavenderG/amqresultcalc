package org.lavenderg.amqresultcalc.io.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lavenderg.amqresultcalc.exceptions.InvalidRecordLengthAMQParserException;
import org.lavenderg.amqresultcalc.logic.mentions.Mention;

/**
 * Clase que procesa menciones y los transforma en datos procesables por la
 * parte lógica de la aplicación.
 * 
 * @author lavenderg
 *
 */
public class MentionParser {

	// Número de elementos de los registros del CSV
	private static final int MENTIONS_RECORD_LENGTH = 1;

	/**
	 * Procesa un archivo con menciones y devuelve una lista de menciones.
	 * 
	 * @param inFile El {@link File} que contiene las menciones.
	 * @return Un {@link List} de {@link Mention}.
	 * @throws FileNotFoundException Lanzada cuando el archivo de menciones no puede
	 *                               ser encontrado.
	 * @throws IOException           Lanzada cuando ocurre un error I/O al leer el
	 *                               archivo de menciones.
	 */
	public List<Mention> parseMentions(File inFile) throws FileNotFoundException, IOException {
		List<Mention> mentions = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(inFile))) {
			while (reader.ready()) {
				String line = reader.readLine();
				String[] lineSplit = line.split(";");
				if (lineSplit.length != MENTIONS_RECORD_LENGTH) {
					throw new InvalidRecordLengthAMQParserException(String
							.format("Registro inválido: '%s'. El registro debe tener el formato: nombreusuario", line));
				}

				String username = lineSplit[0];

				mentions.add(new Mention(username));
			}
		}

		return mentions;
	}
}

package org.lavenderg.amqresultcalc.gui.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.table.AbstractTableModel;

import org.lavenderg.amqresultcalc.logic.result.Result;
import org.lavenderg.amqresultcalc.logic.result.ResultUtil;

public class PlayerResultsTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 5332868247606271572L;

	private final PlayerResultsColumn[] columns = PlayerResultsColumn.values();
	private final List<ResultRow> results = new ArrayList<>();

	public PlayerResultsTableModel(List<Result> results) {
		this();
		if (results != null) {
			this.results.addAll(ResultUtil.orderRowByPoints(
					results.stream().map(result -> new ResultRow(result, true)).collect(Collectors.toList())));
		}
	}

	public PlayerResultsTableModel() {
		this.addTableModelListener(arg0 -> ResultUtil.orderRowByPoints(results));
	}

	@Override
	public int getColumnCount() {
		return this.columns.length;
	}

	@Override
	public int getRowCount() {
		return results.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		var row = results.get(rowIndex);
		var column = columns[columnIndex];
		switch (column) {
		case PUNTUACION:
			return row.getResult().getPlayerPoints();
		case NOMBRE:
			return row.getResult().getPlayerName();
		case POSICION:
			return rowIndex + 1;
		case ESTADO:
			return row.isEnabled();
		default:
			throw new IndexOutOfBoundsException("Invalid column index: " + columnIndex);
		}
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		var row = results.get(rowIndex);
		var column = columns[columnIndex];
		switch (column) {
		case PUNTUACION:
			this.results.set(rowIndex,
					new ResultRow(new Result(row.getResult().getPlayerName(), (Integer) value), row.isEnabled()));
			this.fireTableDataChanged();
			break;
		case ESTADO:
			row.setEnabled(!row.isEnabled());
			this.fireTableDataChanged();
			break;
		default:
			throw new IllegalArgumentException("Cannot set value in column: " + columnIndex);
		}
	}

	/**
	 * Returns the column name for the column index.
	 */
	@Override
	public String getColumnName(int column) {
		return this.columns[column].getColumnName();
	}

	/**
	 * Returns data type of the column specified by its index.
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex).getClass();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columns[columnIndex] == PlayerResultsColumn.PUNTUACION
				|| columns[columnIndex] == PlayerResultsColumn.ESTADO;
	}

	public void insertResult(Result result) {
		this.results.add(new ResultRow(result, true));
		this.fireTableDataChanged();
	}

	public void removeRow(int rowIndex) {
		this.results.remove(rowIndex);
		this.fireTableDataChanged();
	}

	/**
	 * Obtiene los resultados que se usarán en el cálculo de las parejas.
	 * 
	 * @return Los resultados, como una lista de {@link Result}.
	 */
	public List<Result> getResults() {
		return this.results.stream().filter(ResultRow::isEnabled).map(ResultRow::getResult)
				.collect(Collectors.toList());
	}

}

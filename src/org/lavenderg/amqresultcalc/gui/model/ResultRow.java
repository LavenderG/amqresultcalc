package org.lavenderg.amqresultcalc.gui.model;

import org.lavenderg.amqresultcalc.logic.result.Result;

public class ResultRow {

	private Result result;
	private boolean enabled;

	public ResultRow(Result result, boolean enabled) {
		this.result = result;
		this.enabled = enabled;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}

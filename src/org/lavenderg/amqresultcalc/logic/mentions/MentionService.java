package org.lavenderg.amqresultcalc.logic.mentions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.lavenderg.amqresultcalc.io.parser.MentionParser;

/**
 * Clase que ofrece métodos para manejar lógica sobre {@link Mention}.
 * 
 * @author lavenderg
 *
 */
public class MentionService {

	/**
	 * Nombre del archivo predeterminado de menciones.
	 */
	public static final String DEFAULT_MENTIONS_FILE = "menciones.txt";

	private final MentionParser mentionParser = new MentionParser();

	/**
	 * Intenta leer el archivo predeterminado de menciones.
	 * 
	 * @return Una lista de {@link Mention} con las menciones leídas.
	 * @throws FileNotFoundException Lanzada cuando no existe el archivo de
	 *                               menciones.
	 * @throws IOException           Lanzada cuando no se puede leer el archivo de
	 *                               menciones.
	 */
	public List<Mention> tryLoadMentions() throws FileNotFoundException, IOException {
		return mentionParser.parseMentions(new File(DEFAULT_MENTIONS_FILE));

	}
}

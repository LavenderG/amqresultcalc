package org.lavenderg.amqresultcalc.logic.mentions;

/**
 * Clase que representa un usuario para ser mencionado.
 * 
 * @author lavenderg
 */
public class Mention {
	private final String name;

	public Mention(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
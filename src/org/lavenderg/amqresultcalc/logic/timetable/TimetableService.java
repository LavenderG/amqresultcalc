package org.lavenderg.amqresultcalc.logic.timetable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.lavenderg.amqresultcalc.io.parser.TimetableParser;

/**
 * Clase que ofrece métodos para manejar lógica sobre {@link Timetable}.
 * 
 * @author lavenderg
 *
 */
public class TimetableService {

	/**
	 * Nombre del archivo predeterminado de horarios.
	 */
	public static final String DEFAULT_TIMETABLE_FILE = "horarios.txt";

	private final TimetableParser timetableParser = new TimetableParser();

	/**
	 * Intenta leer el archivo predeterminado de horarios.
	 * 
	 * @return Una lista de {@link Timetable} con los horarios leídos.
	 * @throws FileNotFoundException Lanzada cuando no existe el horarios de baneos.
	 * @throws IOException           Lanzada cuando no se puede leer el archivo de
	 *                               horarios.
	 */
	public List<Timetable> tryLoadTimetable() throws FileNotFoundException, IOException {
		return timetableParser.parseTimetable(new File(DEFAULT_TIMETABLE_FILE));

	}
}

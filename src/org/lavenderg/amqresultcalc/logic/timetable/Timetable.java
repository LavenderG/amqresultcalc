package org.lavenderg.amqresultcalc.logic.timetable;

/**
 * Clase que representa una zona horaria de un país utilizada a la hora de
 * mostrar los horarios de la semana que viene.
 * 
 * @author lavenderg
 */
public class Timetable {
	private final String country;
	private final String timezone;
	private final String time;

	public Timetable(String country, String timezone, String time) {
		this.country = country;
		this.timezone = timezone;
		this.time = time;
	}

	public String getCountry() {
		return country;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getTime() {
		return time;
	}
}
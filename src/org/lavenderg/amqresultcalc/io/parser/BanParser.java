package org.lavenderg.amqresultcalc.io.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lavenderg.amqresultcalc.exceptions.InvalidRecordLengthAMQParserException;
import org.lavenderg.amqresultcalc.logic.bans.BannedTag;

/**
 * Clase que procesa tags baneados y los transforma en datos procesables por la
 * parte lógica de la aplicación.
 * 
 * @author lavenderg
 *
 */
public class BanParser {

	// Número de elementos de los registros del CSV
	private static final int BANS_RECORD_LENGTH = 1;

	/**
	 * Procesa un archivo con tags baneados y devuelve una lista de baneos.
	 * 
	 * @param inFile El {@link File} que contiene los tags baneados.
	 * @return Un {@link List} de {@link BannedTag}.
	 * @throws FileNotFoundException Lanzada cuando el archivo de bans no puede ser
	 *                               encontrado.
	 * @throws IOException           Lanzada cuando ocurre un error I/O al leer el
	 *                               archivo de bans.
	 */
	public List<BannedTag> parseBans(File inFile) throws FileNotFoundException, IOException {
		List<BannedTag> bans = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(inFile))) {
			while (reader.ready()) {
				String line = reader.readLine();
				String[] lineSplit = line.split(";");
				if (lineSplit.length != BANS_RECORD_LENGTH) {
					throw new InvalidRecordLengthAMQParserException(String
							.format("Registro inválido: '%s'. El registro debe tener el formato: tagbaneado", line));
				}

				String bannedTag = lineSplit[0];

				bans.add(new BannedTag(bannedTag));
			}
		}

		return bans;
	}
}

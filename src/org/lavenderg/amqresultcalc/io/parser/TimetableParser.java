package org.lavenderg.amqresultcalc.io.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lavenderg.amqresultcalc.exceptions.InvalidRecordLengthAMQParserException;
import org.lavenderg.amqresultcalc.logic.timetable.Timetable;

/**
 * Clase que procesa horarios y los transforma en datos procesables por la parte
 * lógica de la aplicación.
 * 
 * @author lavenderg
 *
 */
public class TimetableParser {

	// Número de elementos de los registros del CSV
	private static final int TIMETABLE_RECORD_LENGTH = 3;

	/**
	 * Procesa un archivo con horarios y devuelve una lista de horarios.
	 * 
	 * @param inFile El {@link File} que contiene los resultados.
	 * @return Un {@link List} de {@link Timetable}.
	 * @throws FileNotFoundException Lanzada cuando el archivo de horarios no puede
	 *                               ser encontrado.
	 * @throws IOException           Lanzada cuando ocurre un error I/O al leer el
	 *                               archivo de horarios.
	 */
	public List<Timetable> parseTimetable(File inFile) throws FileNotFoundException, IOException {
		List<Timetable> timetables = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(inFile))) {
			while (reader.ready()) {
				String line = reader.readLine();
				String[] lineSplit = line.split(";");
				if (lineSplit.length != TIMETABLE_RECORD_LENGTH) {
					throw new InvalidRecordLengthAMQParserException(String.format(
							"Registro inválido: '%s'. El registro debe tener el formato: pais;zonahoraria;hora", line));
				}

				String country = lineSplit[0];
				String timezone = lineSplit[1];
				String time = lineSplit[2];

				timetables.add(new Timetable(country, timezone, time));
			}
		}

		return timetables;
	}
}

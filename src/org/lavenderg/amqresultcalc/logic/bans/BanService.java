package org.lavenderg.amqresultcalc.logic.bans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.lavenderg.amqresultcalc.io.parser.BanParser;

/**
 * Clase que ofrece métodos para manejar lógica sobre {@link BannedTag}.
 * 
 * @author lavenderg
 *
 */
public class BanService {

	/**
	 * Nombre del archivo predeterminado de bans.
	 */
	public static final String DEFAULT_BANS_FILE = "bans.txt";

	private final BanParser banParser = new BanParser();

	/**
	 * Intenta leer el archivo predeterminado de bans.
	 * 
	 * @return Una lista de {@link BannedTag} con los tags leídos.
	 * @throws FileNotFoundException Lanzada cuando no existe el archivo de baneos.
	 * @throws IOException           Lanzada cuando no se puede leer el archivo de
	 *                               baneos.
	 */
	public List<BannedTag> tryLoadBans() throws FileNotFoundException, IOException {
		return banParser.parseBans(new File(DEFAULT_BANS_FILE));

	}
}

package org.lavenderg.amqresultcalc.logic.bans;

/**
 * Clase que representa un tag a banear de AMQ.
 * 
 * @author lavenderg
 */
public class BannedTag {
	private final String tagName;

	public BannedTag(String bannedTag) {
		this.tagName = bannedTag;
	}

	public String getBannedTag() {
		return tagName;
	}
}